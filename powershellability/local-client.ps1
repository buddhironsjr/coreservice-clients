#
# client-connection.ps1
#

 try{
    Write-Host "Preparing"
    $modules = "Tridion-CoreService"
    $modules | foreach {Import-Module $_ -ErrorAction Stop}
    Set-TridionCoreServiceSettings -Version Sites-9.0 -HostName localhost:81
    $client = Get-TridionCoreServiceClient
  }
  catch{
    Write-Host ("Process stopped with error {0}"-f $_ ) -ForegroundColor Red -BackgroundColor Black
    exit
  }