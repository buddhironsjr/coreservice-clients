#
# remote_client.ps1
#
<#
.Description
	sets relevant tridion core service client settings
	Write password by running
	Read-Host -AsSecureString | ConvertFrom-SecureString | Out-File $CredentialFile
#>
[CmdletBinding()]
Param (
    [Parameter(Mandatory=$false)]
	[String]$HostName='18.222.163.173:81',

    [Parameter(Mandatory=$false)]
	[String]$User = 'EC2AMAZ-TUJ3HK0\\Administrator',

	#file system refs are relative to the VS sln file
    [Parameter(Mandatory=$false)]
	[String]$CredentialFile='.\powershellability\credential.txt'
)
Begin {
    Import-Module Tridion-CoreService -Verbose:$true
}

Process {
	# Write password by  running the following line in the solution folder
	# Read-Host -AsSecureString | ConvertFrom-SecureString | Out-File $credentialFile
	# Read-Host -AsSecureString | ConvertFrom-SecureString | Out-File ".\powershellability\credential.txt"

	$credentialPassword = cat $CredentialFile | convertto-securestring                                                            

	$credential = new-object -typename System.Management.Automation.PSCredential -argumentlist $User,$credentialPassword                

	#Set-TridionCoreServiceSettings [[-HostName] <String>] [[-Version] <String>] [[-Credential] <PSCredential>]
    #[[-CredentialType] <String>] [[-ConnectionType] <String>] [[-ConnectionSendTimeout] <String>] [[-AdfsUrl]
    #<String>] [-Persist] [-PassThru] [<CommonParameters>]
	
	Set-TridionCoreServiceSettings -HostName $HostName -ConnectionType Default -CredentialType Windows -Version "Sites-9.0" -Credential $credential
	
	
	$client = Get-TridionCoreServiceClient
	$client.GetCurrentUser()

	Write-Host $client.State
}