﻿#
# UpdateBPT.ps1 by Harald
#

[CmdletBinding()]
Param(
  [Parameter(Mandatory=$false)]
  [string]$BusinessProcessType = 'Corp Preview/Live NEW',

  [Parameter(Mandatory=$false)]
  [string]$TopologyType = 'StagingLive',

  [switch]$DryRun = $false
)

Begin{
  try{
    Write-Host "Preparing"
    $modules = "Tridion-CoreService"
    $modules | foreach {Import-Module $_ -ErrorAction Stop}
    Set-TridionCoreServiceSettings -Version Sites-9.0 -HostName localhost:81
    $client = Get-TridionCoreServiceClient
  }
  catch{
    Write-Host ("Process stopped with error {0}"-f $_ ) -ForegroundColor Red -BackgroundColor Black
    exit
  }
}	
Process{
  try{
  get-cor
    $bptid = $client.GetBusinessProcessTypes($TopologyType) |Where-Object {$_.Title -eq $BusinessProcessType} | foreach{$_.idRef}
    Write-Host ("Updating BPT's for All Publications to {0} ({1})" -f $BusinessProcessType, $bptid)
    $publicationIds = (Get-TtmMapping).PublicationId
    foreach ($publicationId in $publicationIds){
        $pub = $client.Read($publicationId,$null)
    
        $pubid = $pub.id -Replace "tcm:0-([0-9]*)-1",'$1'
        $bptl=  New-Object Tridion.ContentManager.CoreService.Client.LinkToBusinessProcessTypeData
        $bptl.IdRef=($bptid -Replace("tcm:([0-9]*)-",("tcm:{0}-" -f $pubid)))
        $pub.BusinessProcessType = $bptl
      if ($DryRun){
        Write-Host ("Would be Updating BPT for {1} ({0}) to {2}" -f $pub.Id, $pub.Title, $pub.BusinessProcessType.IdRef)
      } 
      else {
        $client.Save($pub,$null)
        Write-Host ("Updated BPT for {1} ({0})" -f $pub.Id, $pub.Title)
    }

}
    Write-Host "Done"
  }
  catch{
    Write-Host ("Process stopped with error {0}"-f $_ ) -ForegroundColor Red -BackgroundColor Black
  }  
}