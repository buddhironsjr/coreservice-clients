﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Tridion.ContentManager.CoreService.Client;

namespace console_w_wrapper
{
    class Program
    {
        const string _uid = "Administrator";
        const string _pwd = "Montreal2018!";
        const string _host = "3.15.218.117";
        const string _domain = "EC2AMAZ-TUJ3HK0";

        static void Main(string[] args)
        {
            using (TextWriter stderr = Console.Error)
            using (TextWriter stdout = Console.Out)
            using (TextReader stdin = Console.In)
            {
                try
                {
                    ClientWrapper wrapper = new ClientWrapper(stdout, stderr, stdin);

                    var client = wrapper.Connect(_host, _uid, _pwd, _domain);

                    stdout.WriteLine($"... version {client.GetApiVersion()}");

                    //probably this is supposed to go into a wrapper method ... now using the refs directly here
                    RepositoryLocalObjectsFilterData filter = new RepositoryLocalObjectsFilterData();
                    XElement checkedOutItemsXml = client.GetSystemWideListXml(filter);
                    stdout.WriteLine($"{checkedOutItemsXml.Nodes().Count()} nodes retreived");
                    
                }
                catch(Exception e)
                {
                    stderr.WriteLine(e.Message);
                    stderr.WriteLine(e.HResult);
                    stderr.WriteLine(e.InnerException);
                }
                finally
                {

                }
            }
        }
    }
}
