﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tridion.ContentManager.CoreService.Client;
using System.ServiceModel;
using System.Net;
using System.Xml;

namespace console_w_wrapper
{
    class ClientWrapper
    {
        private readonly string _service_path = "/webservices/CoreService201701.svc/wsHttp";
        private readonly int _service_port = 81;
        private readonly string _service_scheme = "http";
        //private readonly int _service_port = 443;
        //private readonly string _service_scheme = "https";

        private readonly TextWriter _stdout;
        private readonly TextWriter _stderr;
        private readonly TextReader _stdin;
        //private CommandLineOptions _options;
        internal ClientWrapper(TextWriter stdout, TextWriter stderr, TextReader stdin)
        {
            _stdout = stdout;
            _stderr = stderr;
            _stdin = stdin;
        }

        public CoreServiceClient Connect(string hostname, string uid, string pwd, string domain)
        {
            bool isIntegratedAuth = string.IsNullOrEmpty(uid);

            var binding = new WSHttpBinding
            {
                AllowCookies = true,
                TransactionFlow = true,
                MaxReceivedMessageSize = int.MaxValue,
                ReaderQuotas = new XmlDictionaryReaderQuotas
                {
                    MaxStringContentLength = int.MaxValue,
                    MaxArrayLength = int.MaxValue
                },
                Security = new WSHttpSecurity
                {
                    //None is the only option that allows it to run with http
                    Mode = SecurityMode.Message,
                    Transport = new HttpTransportSecurity
                    {
                        //we will have a uid here, so it will be windows-- 
                        //ClientCredentialType = isIntegratedAuth ? HttpClientCredentialType.Basic : HttpClientCredentialType.Windows
                        //every crednetial type yields error : "The HTTP request is unauthorized with client authentication scheme 'Anonymous'. The authentication header received from the server was 'Negotiate,NTLM'."
                        ClientCredentialType = HttpClientCredentialType.Windows
                    }
                }
            };

            var endpointURL = new UriBuilder(_service_scheme, hostname, _service_port)
            {
                Path = _service_path,
            };

            EndpointAddress endpoint = new EndpointAddress(endpointURL.Uri);
            CoreServiceClient instance = new CoreServiceClient(binding, endpoint);

            //NetworkCredential networkCredential = new NetworkCredential("Administrator", "Montreal2018!", "EC2AMAZ-TUJ3HK0");
            NetworkCredential networkCredential = new NetworkCredential(uid, pwd, domain);
            instance.ClientCredentials.Windows.ClientCredential = networkCredential;

            try
            {
                instance.Open();
                Log($"Connected to {instance.InnerChannel.RemoteAddress} running SDL Tridion v{instance.GetApiVersion()}");
            }
            catch(Exception e)
            {
                Warn(e.Message);
                throw (e);
            }

            return instance;
        }

        private void Log(string message)
        {
            _stdout.WriteLine(message);
        }
        private void Warn(string message)
        {
            _stderr.WriteLine(message);
        }
    }
}
