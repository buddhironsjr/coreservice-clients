﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using Tridion.ContentManager.CoreService.Client;
using System.Net;
using System.ServiceModel;

namespace console_explorer
{
    class Program
    {
        private static string binding = "wsHttp_201701";

        static void Main(string[] args)
        {
            XElement items = FindCheckedOutItemsInChannel();
            //XElement items = FindCheckedOutItemsByClient();

            if(items != null && items.HasElements)
            {
                Log("Elements retreived successfully");

                CheckinItems(items);
            }
            else
            {
                Warn("No Elements!");
            }
        }

        public static XElement FindCheckedOutItemsInChannel()
        {
            Log("Entering FindCheckedOutItemsInChannel()");
            Log("...using ChannelFactory...");

            //use the factory if we need multiple client connections 
            ChannelFactory<ISessionAwareCoreService> factory = new ChannelFactory<ISessionAwareCoreService>(binding);
            
                NetworkCredential networkCredential = new NetworkCredential("Administrator", "Montreal2018!", "EC2AMAZ-TUJ3HK0");
                factory.Credentials.Windows.ClientCredential = networkCredential;
            
                ISessionAwareCoreService client = factory.CreateChannel();
                //following is not necessary-- it will open when needed below
                ((IClientChannel)client).Open();

                ////cast as the correct interface to call methods like these
                //((IClientChannel)client).Abort();
                //((IClientChannel)client).Open();
                ////this will throw an exception in a faulted state
                //((IClientChannel)client).Close();

                //do some work here
                RepositoryLocalObjectsFilterData filter = new RepositoryLocalObjectsFilterData();
                XElement checkedOutItemsXml = client.GetSystemWideListXml(filter);

            //this should handle closing and disposing of any channels created by the factory
            factory.Close();

            return checkedOutItemsXml;
        }
        public static XElement FindCheckedOutItemsByClient()
        {
            Log("Entering FindCheckedOutItemsByClient()");
            Log("...using single Client...");

            SessionAwareCoreServiceClient client = new SessionAwareCoreServiceClient(binding);

            NetworkCredential networkCredential = new NetworkCredential("Administrator", "Montreal2018!", "EC2AMAZ-TUJ3HK0");
            client.ClientCredentials.Windows.ClientCredential = networkCredential;

            //we are not using this because if there is a network connection error we will never catch it
            //  and risk not properly disposing of the connection if it is in a faulted state
            //using (SessionAwareCoreServiceClient client = new SessionAwareCoreServiceClient(binding))
            //{
            //    RepositoryLocalObjectsFilterData filter = new RepositoryLocalObjectsFilterData();
            //    XElement checkedOutItemsXml = client.GetSystemWideListXml(filter);
            //    return checkedOutItemsXml;
            //}

            try
            {
                RepositoryLocalObjectsFilterData filter = new RepositoryLocalObjectsFilterData();
                XElement checkedOutItemsXml = client.GetSystemWideListXml(filter);

                return checkedOutItemsXml;
            }
            catch (Exception e)
            {
                //will throw a System.ServiceModel.Security.SecurityNegotiationException if the credentials fail to authenticate

                Error(e.Message);
                Error(e.InnerException.Message);
            }
            finally
            {
                //dispose of client regardless of state
                if(client != null)
                {
                    if(client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        client.Abort();
                    }
                    else
                    {
                        client.Close();
                    }

                    if (client is IDisposable disposable)
                        disposable.Dispose();
                }
            }

            return null;
        }

        private static void CheckinItems(XElement items)
        {
            Log("Entering CheckinItems()");

            //using (SessionAwareCoreServiceClient client = new SessionAwareCoreServiceClient(binding))
            //{
                var i = 0;
                foreach(XElement tridionItem in items.Nodes())
                {
                    i++;
                    if(tridionItem.Attribute("Type").Value == "16" || tridionItem.Attribute("Type").Value == "64")
                    {
                        //client.CheckIn(tridionItem.Attribute("ID").Value, new ReadOptions());
                    }
                }

                Log($"{i} elements retreived.");
            //}
        }

        private static void Log(string message)
        {
            System.Diagnostics.Trace.WriteLine($"LOG  {message}");
            Console.WriteLine($"LOG  {message}");
        }
        private static void Warn(string message)
        {
            System.Diagnostics.Trace.WriteLine($"WARN {message}");
            Console.WriteLine($"WARN {message}");
        }
        private static void Error(string message)
        {
            System.Diagnostics.Trace.WriteLine($"ERROR {message}");
            Console.WriteLine($"ERROR {message}");
        }
    }
}
