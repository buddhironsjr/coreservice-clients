﻿using service_ref_test.CoreService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace service_ref_test
{
    class Program
    {
        static void Main(string[] args)
        {
            //CoreService.CoreServiceClient client = new CoreService.CoreServiceClient();

            using (ChannelFactory<ISessionAwareCoreService> factory = new ChannelFactory<ISessionAwareCoreService>("wsHttp"))
            {
                NetworkCredential networkCredential = new NetworkCredential("Administrator", "Montreal2018!", "EC2AMAZ-TUJ3HK0");
                factory.Credentials.Windows.ClientCredential = networkCredential;
                ISessionAwareCoreService client = factory.CreateChannel();
              
                Console.WriteLine(client.GetCurrentUser().Title);

                Console.WriteLine(client.GetApiVersion());

                PublicationsFilterData publicationsFilter = new PublicationsFilterData();
                XElement publications = client.GetSystemWideListXml(publicationsFilter);
            }
        }
    }
}
