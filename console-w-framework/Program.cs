﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using GenericCoreServiceFramework.Framework;
using Tridion.ContentManager.CoreService.Client;

namespace console_w_framework
{
    class Program
    {
        // http://3.15.218.117:81/webservices/CoreService201701.svc/wsHttp
        const string _uid = "Administrator";
        const string _pwd = "Montreal2018!";
        const string _host = "18.188.110.232";
        const string _domain = "EC2AMAZ-TUJ3HK0";
        const string _scheme = "http";
        const int _port = 81;
        const string _path = "/webservices/CoreService201701.svc/wsHttp";
        static void Main(string[] args)
        {
            var endpointURL = new UriBuilder(_scheme, _host, _port)
            {
                Path = _path
            };
            NetworkCredential networkCredential = new NetworkCredential(_uid, _pwd, _domain);

            var client = CoreServiceFactory.GetWsHttpContext(endpointURL.Uri, networkCredential).Client;

            Console.WriteLine($"Connected to {endpointURL.Uri} running v{client.GetApiVersion()}");

            RepositoryLocalObjectsFilterData filter = new RepositoryLocalObjectsFilterData();
            XElement checkedOutItemsXml = client.GetSystemWideListXml(filter);
            Console.WriteLine($"{checkedOutItemsXml.Nodes().Count()} nodes retreived");
        }
    }
}
